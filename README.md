# Shoulder-Lombok

scm: [gitee](https://gitee.com/ChinaLym/shoulder-lombok) or [github](https://github.com/ChinaLym/shoulder-lombok)

## a [lombok](https://github.com/rzwitserloot/lombok) ext for shoulder-framework.

- add a special annotation(`@SLog`) for Simplify use `shoulder-framework`.

## Usage ?

### Maven

add this `dependency` into your `pom.xml`

```
        <dependency>
            <groupId>cn.itlym.shoulder</groupId>
            <artifactId>lombok</artifactId>
            <version>0.1</version>
        </dependency>
```
or 
```
    <properties>
        <!-- shoulder-lombok -->
        <shoulder-lombok.version>0.1</shoulder-lombok.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>cn.itlym.shoulder</groupId>
            <artifactId>lombok</artifactId>
            <version>${shoulder-lombok.version}</version>
        </dependency>
    </dependencies>
```

## Better experience in `Intellij IDEA`

#### Download and install **shoulder lombok-intellij-plugin** from **[gitee](https://gitee.com/ChinaLym/lombok-intellij-plugin)** or **[github](https://github.com/ChinaLym/lombok-intellij-plugin)**


### what will happen when use `@SLog` before your class ?

It will auto generate code like this while compiling.

```java
private static final org.shoulder.core.log.Logger log = org.shoulder.core.log.LoggerFactory.getLogger(TargetType.class);
````

![pic](shoulder-lombok-Logger.png)

---

To better use it in IDEA, see 




 